using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushBox : MonoBehaviour
{
    public Transform targetPosition; // Posici�n objetivo para el puzzle
    private bool isAtTarget = false; // Bandera para comprobar si la caja est� en la posici�n objetivo

    void Update()
    {
        if (targetPosition != null)
        {
            // Comprobar si la caja est� cerca de la posici�n objetivo
            if (Vector3.Distance(transform.position, targetPosition.position) < 0.5f && !isAtTarget)
            {
                isAtTarget = true;
                OnPuzzleSolved();
            }
        }
    }

    void OnPuzzleSolved()
    {
        // Acci�n a realizar cuando la caja est� en la posici�n objetivo
        Debug.Log("Puzzle solved!");
        // Aqu� puedes agregar l�gica adicional para cuando se resuelva el puzzle, como abrir una puerta, activar una plataforma, etc.
    }
}
