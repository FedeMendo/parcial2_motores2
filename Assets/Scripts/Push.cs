using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; // Importar la librer�a UI para usar la clase Image

public class Push : MonoBehaviour
{
    public float forceappl = 5f; // Force applied when pushing
    public float rayDistance = 0.13f; // Distance for the raycast
    public Image boxDetectedImage; // Referencia a la imagen que se activar�
    private Animator anim;
    private Rigidbody rb;
    private bool isPushing = false;
    private GameObject currentBox = null; // The current box being pushed

    void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();

        if (anim == null)
        {
            Debug.LogError("Animator no encontrado en el objeto principal.");
        }

        // Aseg�rate de que la imagen est� desactivada al inicio
        if (boxDetectedImage != null)
        {
            boxDetectedImage.gameObject.SetActive(false);
        }
        else
        {
            Debug.LogError("Referencia de imagen no asignada.");
        }
    }

    void Update()
    {
        // Check for box detection using raycasts
        DetectBox();

        // Check if the "E" key is held down and a box is detected
        if (isPushing && Input.GetKey(KeyCode.E))
        {
            anim.SetBool("IsPushing", true);

            // Allow the box to move
            if (currentBox != null)
            {
                Rigidbody boxRb = currentBox.GetComponent<Rigidbody>();
                if (boxRb != null)
                {
                    boxRb.isKinematic = false; // Enable physics interaction

                    // Move the box along with the player
                    float horizontalInput = Input.GetAxis("Horizontal");
                    Vector3 playerMovement = new Vector3(horizontalInput * forceappl * Time.deltaTime, 0, 0);
                    boxRb.velocity = new Vector3(playerMovement.x, boxRb.velocity.y, boxRb.velocity.z);

                    // Move the player in the same direction as the box
                    rb.velocity = new Vector3(playerMovement.x, rb.velocity.y, rb.velocity.z);
                }
            }
        }
        else
        {
            anim.SetBool("IsPushing", false);

            // Set the box to kinematic when not pushing
            if (currentBox != null)
            {
                Rigidbody boxRb = currentBox.GetComponent<Rigidbody>();
                if (boxRb != null)
                {
                    boxRb.isKinematic = true; // Prevent any movement
                }
            }
        }
    }

    void DetectBox()
    {
        RaycastHit hit;
        bool boxDetected = false;

        // Raycast to the right
        if (Physics.Raycast(transform.position, Vector3.right, out hit, rayDistance))
        {
            if (hit.collider.CompareTag("Box"))
            {
                isPushing = true;
                currentBox = hit.collider.gameObject;
                boxDetected = true;
            }
        }

        // Raycast to the left
        if (Physics.Raycast(transform.position, Vector3.left, out hit, rayDistance))
        {
            if (hit.collider.CompareTag("Box"))
            {
                isPushing = true;
                currentBox = hit.collider.gameObject;
                boxDetected = true;
            }
        }

        // Activar o desactivar la imagen seg�n si se detect� una caja
        if (boxDetectedImage != null)
        {
            boxDetectedImage.gameObject.SetActive(boxDetected);
        }

        if (!boxDetected)
        {
            isPushing = false;
            currentBox = null;
        }
    }
}
