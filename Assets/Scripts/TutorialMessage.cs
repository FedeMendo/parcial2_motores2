using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TutorialMessage : MonoBehaviour
{
    public Image tutorialImage; // Referencia al componente Image del cartel

    void Start()
    {
        // Desactivar la imagen del tutorial al inicio
        if (tutorialImage != null)
        {
            tutorialImage.gameObject.SetActive(false);
        }
        else
        {
            Debug.LogError("No se ha asignado el componente Image en el Inspector.");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            tutorialImage.gameObject.SetActive(true); // Activar la imagen del tutorial al entrar en el collider
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            tutorialImage.gameObject.SetActive(false); // Desactivar la imagen del tutorial al salir del collider
        }
    }
}