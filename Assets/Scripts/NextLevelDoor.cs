using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Control_Player player = other.GetComponent<Control_Player>();
            if (player != null && player.HasKey)
            {
                player.MoveToNextLevel();
            }
        }
    }
}
