using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class Control_Player : MonoBehaviour
{
    public float moveSpeed = 5f;
    public float jumpForce = 7f;
    public int maxLives = 3; // Cambiado a 3 vidas
    public Image lifeImage; // Image UI para mostrar el sprite de vida
    public Sprite threeLivesSprite; // Sprite para 3 vidas
    public Sprite twoLivesSprite; // Sprite para 2 vidas
    public Sprite oneLifeSprite; // Sprite para 1 vida
    public Sprite noLivesSprite; // Sprite para 0 vidas (Game Over)
    public TextMeshProUGUI gameOverText;
    public Transform respawnPoint; // Punto de respawn original
    public Transform nextLevelSpawnPoint; // Punto de respawn del siguiente nivel
    public bool HasKey { get; set; } = false; // Estado de la llave

    private int currentLives;
    private Rigidbody rb;
    private bool gameOver = false;

    private Animator animator;
    private bool facingRight = true; // Bandera para controlar la direcci�n en la que est� mirando el jugador
    public bool isDead;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        currentLives = maxLives;
        UpdateLifeImage();
        gameOverText.gameObject.SetActive(false);
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (isDead)
        {
           if(Input.GetKeyDown(KeyCode.R)) 
            {
                RestartGame();  
            }
            return;
        }
        if (!gameOver)
        {
            // Movimiento horizontal
            float moveInput = Input.GetAxis("Horizontal");
            Vector3 movement = new Vector3(moveInput * moveSpeed, rb.velocity.y, 0);
            rb.velocity = movement;

            animator.SetFloat("Horizontal", Mathf.Abs(moveInput));
            animator.SetBool("InFloor", IsGrounded());

            // Voltear el jugador si es necesario
            if (moveInput > 0 && !facingRight)
            {
                Flip();
            }
            else if (moveInput < 0 && facingRight)
            {
                Flip();
            }

            // Saltar
            if (Input.GetKeyDown(KeyCode.W) && IsGrounded())
            {
                rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            }
        }
    }

    bool IsGrounded()
    {
        // M�todo para verificar si el jugador est� en el suelo
        return Physics.Raycast(transform.position, Vector3.down, 0.15f);
    }

    void OnTriggerEnter(Collider other)
    {
        if (!gameOver && other.CompareTag("Enemy"))
        {
            TakeDamage();
        }
        if (other.CompareTag("Door") && HasKey)
        {
            MoveToNextLevel();
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (!gameOver && collision.gameObject.CompareTag("Lava"))
        {
            TakeDamage(); // Llamar a TakeDamage si el jugador entra en contacto con la lava
        }
    }

    void TakeDamage()
    {
        currentLives--;
        Debug.Log("Lives left: " + currentLives);
        UpdateLifeImage();

        if (currentLives <= 0)
        {
            Debug.Log("Game Over!");
            ShowGameOverText();
            gameOver = true;
            animator.SetTrigger("IsDie");
            isDead = true;
        }
        else
        {
            Respawn();
        }
    }

    void UpdateLifeImage()
    {
        switch (currentLives)
        {
            case 3:
                lifeImage.sprite = threeLivesSprite;
                break;
            case 2:
                lifeImage.sprite = twoLivesSprite;
                break;
            case 1:
                lifeImage.sprite = oneLifeSprite;
                break;
            default:
                lifeImage.sprite = noLivesSprite;
                break;
        }
    }

    void ShowGameOverText()
    {
        gameOverText.gameObject.SetActive(true);
        gameOverText.text = "Game Over!";

    }

    public void AddLives(int amount)
    {
        currentLives += amount;
        currentLives = Mathf.Clamp(currentLives, 0, maxLives);
        Debug.Log("Lives added: " + amount);
        UpdateLifeImage();
    }

    public int GetCurrentLives()
    {
        return currentLives;
    }

    void Respawn()
    {
        transform.position = respawnPoint.position;
        rb.velocity = Vector3.zero;
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    void OnDestroy()
    {
        Time.timeScale = 1f;
    }

    public void MoveToNextLevel()
    {
        // Mover al jugador al punto de respawn del siguiente nivel
        transform.position = nextLevelSpawnPoint.position;
        rb.velocity = Vector3.zero;

        // Actualizar el respawnPoint al nuevo nivel
        respawnPoint = nextLevelSpawnPoint;
    }

    void Flip()
    {
        // Don't flip if the "E" key is being pressed
        if (Input.GetKey(KeyCode.E))
        {
            return;
        }

        // Cambiar la direcci�n en la que est� mirando el jugador
        facingRight = !facingRight;
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }
}
