using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TriggerTextActivator : MonoBehaviour
{
    public TextMeshProUGUI triggerText;
    public string targetTag = "Player";

    void Start()
    {
        if (triggerText != null)
        {
            triggerText.gameObject.SetActive(false);
        }
        else
        {
            Debug.LogError("Referencia de TextMeshProUGUI no asignada.");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(targetTag))
        {
            if (triggerText != null)
            {
                triggerText.gameObject.SetActive(true);
            }

            Time.timeScale = 0f; // Pause the game
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(targetTag))
        {
            if (triggerText != null)
            {
                triggerText.gameObject.SetActive(false);
            }

            Time.timeScale = 1f; // Resume the game
        }
    }
}
