using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxEffect : MonoBehaviour
{
    private float startPosX;
    private Transform camTransform;
    public float parallaxEffect;

    void Start()
    {
        startPosX = transform.position.x;
        camTransform = Camera.main.transform;
    }

    void Update()
    {
        // Calcular la cantidad de movimiento en el eje X basado en la posici�n de la c�mara
        float distX = (camTransform.position.x - startPosX) * parallaxEffect;

        // Mantener la posici�n Y fija
        float posX = startPosX + distX;
        float posY = transform.position.y; // Mantener la posici�n Y actual

        // Actualizar la posici�n del fondo
        transform.position = new Vector3(posX, posY, transform.position.z);
    }
}