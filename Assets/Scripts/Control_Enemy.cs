using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control_Enemy : MonoBehaviour
{
    public Transform pointA; // Punto A
    public Transform pointB; // Punto B
    public float speed = 2f; // Velocidad de movimiento

    private Vector3 targetPosition; // Posici�n objetivo actual

    void Start()
    {
        targetPosition = pointA.position; // Inicialmente, moverse hacia el punto A
    }

    void Update()
    {
        // Mover al enemigo hacia la posici�n objetivo
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);

        // Si el enemigo ha llegado a la posici�n objetivo
        if (transform.position == targetPosition)
        {
            // Cambiar la posici�n objetivo al otro punto
            if (targetPosition == pointA.position)
            {
                targetPosition = pointB.position;
            }
            else
            {
                targetPosition = pointA.position;
            }
        }
    }
}
