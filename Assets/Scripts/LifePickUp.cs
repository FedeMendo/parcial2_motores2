using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Life : MonoBehaviour
{
    public int lifeAmount = 1; // Cantidad de vidas que este objeto proporciona

    void OnTriggerEnter(Collider other)
    {
        Control_Player player = other.GetComponent<Control_Player>();
        if (player != null)
        {
            if (player.GetCurrentLives() < player.maxLives) // Verificar si el jugador no tiene el m�ximo de vidas
            {
                player.AddLives(lifeAmount);
                Destroy(gameObject); // Destruir el objeto despu�s de recogerlo
            }
            else
            {
                Debug.Log("Vidas llenas, no se puede recoger m�s vidas.");
            }
        }
    }
}
