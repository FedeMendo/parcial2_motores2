using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyPickUp : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Control_Player control_Player = other.GetComponent<Control_Player>();
            if (control_Player != null)
            {
                control_Player.HasKey = true;
                Destroy(gameObject); // Destruir la llave
            }
        }
    }
}
