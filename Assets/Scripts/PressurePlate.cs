using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressurePlate : MonoBehaviour
{
    public GameObject door; // Referencia al GameObject de la puerta que se abrir�

    private bool isOpen = false;

    void OnTriggerEnter(Collider other)
    {
        if (!isOpen && (other.CompareTag("Player") || other.CompareTag("Box")))
        {
            OpenDoor();
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (isOpen && (other.CompareTag("Player") || other.CompareTag("Box")))
        {
            CloseDoor();
        }
    }

    void OpenDoor()
    {
        door.SetActive(false); // Desactiva la puerta (o cualquier otro comportamiento que la abra)
        isOpen = true;
    }

    void CloseDoor()
    {
        door.SetActive(true); // Activa la puerta (o cualquier otro comportamiento que la cierre)
        isOpen = false;
    }
}