using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public Transform pointA;
    public Transform pointB;
    public float moveSpeed = 3f;

    private Vector3 nextPosition;
    private bool movingToA;

    private Transform playerOnPlatform; // Referencia al jugador que est� sobre la plataforma

    void Start()
    {
        nextPosition = pointB.position;
        movingToA = false;
    }

    void Update()
    {
        MovePlatform();
    }

    void MovePlatform()
    {
        transform.position = Vector3.MoveTowards(transform.position, nextPosition, moveSpeed * Time.deltaTime);

        if (transform.position == pointA.position)
        {
            nextPosition = pointB.position;
            movingToA = false;
        }
        else if (transform.position == pointB.position)
        {
            nextPosition = pointA.position;
            movingToA = true;
        }
    }

    void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            playerOnPlatform = collision.transform;
            playerOnPlatform.SetParent(transform);
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            playerOnPlatform.SetParent(null);
            playerOnPlatform = null;
        }
    }
}
